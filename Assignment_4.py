#!/usr/bin/env python
# coding: utf-8

# In[9]:


get_ipython().system('ls -l')


# In[10]:


var = get_ipython().getoutput('ls -l')
type(var)


# In[11]:


#var.fields?


# In[12]:


var.grep("data")


# In[13]:


from google.colab import drive
drive.mount('/content/gdrive', force_remount=True)


# In[14]:


import os;os.listdir("/content/gdrive/My Drive/awsml")


# In[15]:


too_many_decimals = 1.912345897

print("built in Python Round")
get_ipython().run_line_magic('timeit', 'round(too_many_decimals, 2)')


# In[16]:


alias lscsv ls -l sample_data/*.csv 


# In[17]:


lscsv


# In[18]:


var1=1


# In[19]:


who


# In[20]:


too_many_decimals


# In[21]:


get_ipython().run_cell_magic('writefile', 'magic_stuff.py', 'import pandas as pd\ndf = pd.read_csv(\n    "https://raw.githubusercontent.com/noahgift/food/master/data/features.en.openfoodfacts.org.products.csv")\ndf.drop(["Unnamed: 0", "exceeded", "g_sum", "energy_100g"], axis=1, inplace=True) #drop two rows we don\'t need\ndf = df.drop(df.index[[1,11877]]) #drop outlier\ndf.rename(index=str, columns={"reconstructed_energy": "energy_100g"}, inplace=True)\nprint(df.head())\n')


# In[22]:


cat magic_stuff.py


# In[23]:


import pandas as pd
df = pd.read_csv(
    "https://raw.githubusercontent.com/noahgift/food/master/data/features.en.openfoodfacts.org.products.csv")
df.drop(["Unnamed: 0", "exceeded", "g_sum", "energy_100g"], axis=1, inplace=True) #drop two rows we don't need
df = df.drop(df.index[[1,11877]]) #drop outlier
df.rename(index=str, columns={"reconstructed_energy": "energy_100g"}, inplace=True)
print(df.head())


# In[24]:


get_ipython().system('python magic_stuff.py')


# In[27]:


get_ipython().system('pip install -q pylint')


# In[28]:


get_ipython().system('pylint magic_stuff.py')


# In[29]:


get_ipython().run_cell_magic('bash', '', 'uname -a\nls\nps\n')


# In[30]:


get_ipython().system('uname -a')


# In[31]:


get_ipython().run_cell_magic('python2', '', 'print "old school"\n')


# In[32]:


get_ipython().run_cell_magic('html', '', 'Only The Best Tags and People\n')


# In[40]:


from google.colab import auth
auth.authenticate_user()

import gspread
from oauth2client.client import GoogleCredentials

gc = gspread.authorize(GoogleCredentials.get_application_default())

worksheet = gc.open('DevOp-Wonder').sheet1

# get_all_values gives a list of rows.
rows = worksheet.get_all_values()
print(rows)

# Convert to a DataFrame and render.
import pandas as pd
df = pd.DataFrame.from_records(rows)
df


# In[34]:


df.median()


# In[35]:


from google.colab import files
uploaded = files.upload()


# In[39]:


import pandas as pd
df = pd.read_csv("automl-tables_notebooks_census_income.csv")
df.head(2)


# In[38]:


import pandas as pd
df = pd.read_csv("https://raw.githubusercontent.com/noahgift/sugar/master/data/education_sugar_cdc_2003.csv")
df.head()


# In[41]:


df.describe()


# In[42]:


Use_Python = False #@param ["False", "True"] {type:"raw"}


# In[43]:


print(f"You select it is {Use_Python} you use Python")


# In[44]:


get_ipython().system('python -c "import os;print(os.listdir())"')


# In[45]:


get_ipython().system('ls -l')


# In[46]:


ls -l


# In[47]:


print("Hello world")


# In[48]:


variable = "armbar"
variable


# In[49]:


attack_one = "kimura"
attack_two = "arm triangle"
print("In Brazilian Jiu Jitsu a common attack is a:", attack_one)
print("Another common attack is a:", attack_two)


# In[50]:


1+1


# In[51]:


"arm" + " bar"+" 4"+" morestuff " + "lemon"


# In[52]:


belts = ["white", "blue", "purple", "brown", "black"]
for belt in belts:
    if "black" in belt:
        print("The belt I want to be is:", belt)
    else:
        print("This is not the belt I want to end up at:", belt)


# In[53]:


my_string = "this is a string I am using this time"
#my_string.split()
#my_string.upper()
#my_string.title()
#my_string.count("this")


# In[54]:


my_string.title()


# In[55]:


my_string.capitalize()


# In[56]:


my_string.isnumeric()


# In[57]:


print(my_string)
var2 = my_string.swapcase()
print(var2)
print(var2.swapcase())


# In[58]:


basic_string = "Brazilian Jiu Jitsu"


# In[59]:


#split on spaces (default)
basic_string.split()


# In[60]:


result = basic_string.split()
len(result)


# In[62]:


#split on hyphen
string_with_hyphen = "Brazilian-Jiu-Jitsu"
string_with_hyphen.split("-")


# In[63]:


#split on comma
string_with_hyphen = "Brazilian,Jiu,Jitsu"
string_with_hyphen.split(",")


# In[64]:


basic_string.capitalize()


# In[65]:


#Get the last character
basic_string[-1:]


# In[66]:


len(basic_string[2:])


# In[67]:


#Get length of string
len(basic_string)


# In[68]:


basic_string[-18:]


# In[69]:


basic_string + " is my favorite Martial Art"


# In[70]:


items = ["-",1,2,3]
for item in items:
  basic_string += str(item)
basic_string


# In[71]:


"this is a string format: %s" % "wow"


# In[72]:


f'I love practicing my favorite Martial Art, {basic_string}'


# In[73]:


f"""
This phrase is multiple sentenances long.
There phrase can be formatted like simpler sentances,
for example, I can still talk about my favorite Martial Art {basic_string}
"""


# In[74]:


f"""
This phrase is multiple sentenances long.
There phrase can be formatted like simpler sentances,
for example, I can still talk about my favorite Martial Art {basic_string}
""".replace("\n", " ")


# In[75]:


steps = (1+1)-1
print(f"Two Steps Forward:  One Step Back = {steps}")


# In[76]:


body_fat_percentage = 0.10
weight = 200
fat_total = body_fat_percentage * weight
print(f"I weight 200lbs, and {fat_total}lbs of that is fat")


# In[77]:


from decimal import (Decimal, getcontext)

getcontext().prec = 5
Decimal(1)/Decimal(3)


# In[78]:


import math
math.pow(2,4)


# In[79]:


2**4


# In[80]:


2**4


# In[81]:


2*3


# In[82]:


number = 100
num_type = type(number).__name__
print(f"{number} is type [{num_type}]")


# In[83]:


number = float(100)
num_type = type(number).__name__
print(f"{number} is type [{num_type}]")


# In[84]:


num2 = 100.20
type(num2)


# In[85]:


class Foo:pass
f = Foo()


# In[86]:


type(f)


# In[87]:


too_many_decimals = 1.912345897
round(too_many_decimals, 6)
#get more info
#round?


# In[88]:


import numpy as np
np.round(too_many_decimals, 6)


# In[89]:


import pandas as pd
df = pd.DataFrame([too_many_decimals], columns=["A"], index=["first"])
df.round(2)


# In[91]:


print("built in Python Round")
get_ipython().run_line_magic('timeit', '-r 3 -t round(too_many_decimals, 2)')

print("numpy round")
get_ipython().run_line_magic('timeit', '-r 3 -t np.round(too_many_decimals, 2)')

print("Pandas DataFrame round")
get_ipython().run_line_magic('timeit', '-r 3 -t df.round(2)')


# In[92]:


#bad_dictionary = {[2]:"one"}


# In[93]:


new_dictionary = {"one":1}


# In[94]:


submissions = {"armbar": "upper_body", 
               "arm_triangle": "upper_body", 
               "heel_hook": "lower_body", 
               "knee_bar": "lower_body"}
#type(submissions)
#submissions.items?
submissions


# In[95]:


new_dict =dict(upper_body="lower_body")
new_dict


# In[96]:


#submissions.items?


# In[97]:


for submission, body_part in submissions.items():
    print(f"The {submission} is an attack on the {body_part}")


# In[98]:


for _, body_parts in submissions.items():
  print(body_parts)


# In[99]:


print(f"These are lower_body submission attacks in Brazilian Jiu Jitsu:")
for submission, body_part in submissions.items():
    if body_part == "lower_body":
        print(submission)


# In[100]:


print(f"These are keys: {submissions.keys()}")
print(f"These are values: {submissions.values()}")


# In[101]:


if "armbar" in submissions:
  print("found key")


# In[102]:


print("timing key membership")
get_ipython().run_line_magic('timeit', 'if "armbar" in submissions: pass')


# In[103]:


list_of_bjj_positions = ["mount", "full-guard", "half-guard", 
                         "turtle", "side-control", "rear-mount", 
                         "knee-on-belly", "north-south", "open-guard"]


# In[104]:


bjj_dominant_positions = list()
bjj_dominant_positions.append("side-control")
bjj_dominant_positions.append("mount")
bjj_dominant_positions


# In[105]:


guards = "full, half, open"
guard_list = [f"{guard}-guard" for guard in guards.split(",")]
guard_list


# In[106]:


for position in list_of_bjj_positions:
    if "open" in position: #explore on your own "guard"
        print(position)


# In[107]:


print(f'First position: {list_of_bjj_positions[:1]}')
print(f'Last position: {list_of_bjj_positions[-1:]}')
print(f'First three positions: {list_of_bjj_positions[0:3]}')


# In[108]:


bjj_position_matrix = [
    ["dominant", "top-mount", "back-mount", "side-control"],
    ["neutral", "open-guard", "full-guard", "standing"],
    ["weak", "turtle", "bottom-back-mount", "bottom-mount"]
]
list(zip(*bjj_position_matrix))


# In[109]:


get_ipython().run_line_magic('pinfo', 'zip')


# In[110]:


unique_attacks = set(("armbar","armbar", "armbar", "kimura", "kimura", "heel hook"))
print(type(unique_attacks))
unique_attacks


# In[111]:


attacks_set_one = set(("armbar", "kimura", "heal-hook"))
attacks_set_two = set(("toe-hold", "knee-bar", "heal-hook"))
unique_set_one_attacks = attacks_set_one - attacks_set_two
print(f"Unique Set One Attacks {unique_set_one_attacks}")


# In[112]:


good = {'one': 'two'}


# In[113]:


# A list is mutable
var = ["two"]
badone = {var:"two"}


# In[114]:


# be careful about mutating objects
class Foo:pass
foo = Foo()

bad_two = {foo: "two"}


# In[115]:


# try out why this is weird

def fun():pass
bad_three = {fun:"one"}


# In[116]:


def favorite_martial_art():
    return "bjj"


# In[117]:


print(favorite_martial_art())
# This is the same output
my_variable = "bjj"
my_variable


# In[118]:


def myfunc():pass


# In[119]:


res = myfunc()
print(res)
#result = myfunc()
#print(result)


# In[120]:


def favorite_martial_art_with_docstring():
    """This function returns the name of my favorite martial art
    This is more
    This is even more
    return "string"
    """
    return "bjj"


# In[121]:


get_ipython().run_line_magic('pinfo', 'favorite_martial_art_with_docstring')
Signature: favorite_martial_art_with_docstring()
Docstring: This function returns the name of my favorite martial art
File:      ~/src/functional_intro_to_python/<ipython-input-1-bef983c31735>
Type:      function


# In[122]:


get_ipython().run_line_magic('pinfo', 'favorite_martial_art_with_docstring')
Signature: favorite_martial_art_with_docstring()
Docstring: This function returns the name of my favorite martial art
File:      ~/src/functional_intro_to_python/<ipython-input-1-bef983c31735>
Type:      function


# In[123]:


def favorite_martial_art_with_docstring():
    """Returns the name of my favorite martial art."""


# In[124]:


get_ipython().run_line_magic('pinfo', 'favorite_martial_art_with_docstring')
Signature: favorite_martial_art_with_docstring()
Docstring: This function returns the name of my favorite martial art
File:      ~/src/functional_intro_to_python/<ipython-input-1-bef983c31735>
Type:      function


# In[125]:


def favorite_martial_art_with_docstring():
    """This function returns the name of my favorite martial art
    This is more
    This is even more
    return "string"
    """
    return "bjj"


# In[126]:


favorite_martial_art_with_docstring.__doc__
#favorite_martial_art_with_docstring?


# In[127]:


#favorite_martial_art_with_docstring?


# In[128]:


def practice(times):
    print(f"I like to practice {times} times a day")


# In[129]:


practice(2)


# In[130]:


practice(3)


# In[131]:


def practice(times, technique, duration):
    print(f"I like to practice {technique}, {times} times a day, for {duration} minutes")


# In[132]:


practice(3, "piano", 45)


# In[133]:


#Order is important, now the entire is incorrect and prints out nonsense
practice("piano", 7,60)


# In[134]:


def practice(times=2, technique="python", duration=60):
    print(f"I like to practice {technique}, {times} times a day, for {duration} minutes")


# In[135]:


practice()


# In[136]:


practice(duration=90, times=4)


# In[137]:


def attack_techniques(**kwargs):
    """This accepts any number of keyword arguments"""
    
    for name, attack in kwargs.items():
        print(f"This is an attack I would like to practice: {attack}")


# In[138]:


attack_techniques(arm_attack="kimura", 
                  leg_attack="straight_ankle_lock", 
                  neck_attack="arm_triangle",
                 body_attack="charge")


# In[139]:


#I also can pass as many things as I wants
attack_techniques(arm_attack="kimura", 
                  leg_attack="straight_ankle_lock", 
                  neck_attach="arm_triangle",
                  attack4="rear nake choke", attack5="key lock")


# In[140]:


attacks = {"arm_attack":"kimura", 
           "leg_attack":"straight_ankle_lock", 
           "neck_attach":"arm_triangle"}


# In[141]:


attack_techniques(**attacks)


# In[142]:


def attack_location(technique):
    """Return the location of an attack"""
    
    attacks = {"kimura": "arm_attack",
           "straight_ankle_lock":"leg_attack", 
           "arm_triangle":"neck_attach"}
    if technique in attacks:
        return attacks[technique]
    return "Unknown"


# In[143]:


attack_location("kimura")


# In[144]:


attack_location("bear hug")


# In[145]:


def multiple_attacks(attack_location_function):
    """Takes a function that categorizes attacks and returns location"""
    
    new_attacks_list = ["rear_naked_choke", "americana", "kimura"]
    for attack in new_attacks_list:
        attack_location = attack_location_function(attack)
        print(f"The location of attack {attack} is {attack_location}")


# In[146]:


multiple_attacks(attack_location)


# In[147]:


#nonlocal cannot modify this variable
#lower_body_counter=5
def attack_counter():
    """Counts number of attacks on part of body"""
    lower_body_counter = 0
    upper_body_counter = 0
    #print(lower_body_counter)
    def attack_filter(attack):
        nonlocal lower_body_counter
        nonlocal upper_body_counter
        attacks = {"kimura": "upper_body",
           "straight_ankle_lock":"lower_body", 
           "arm_triangle":"upper_body",
            "keylock": "upper_body",
            "knee_bar": "lower_body"}
        if attack in attacks:
            if attacks[attack] == "upper_body":
                upper_body_counter +=1
            if attacks[attack] == "lower_body":
                lower_body_counter +=1
        print(f"Upper Body Attacks {upper_body_counter}, Lower Body Attacks {lower_body_counter}")
    return attack_filter


# In[148]:


fight = attack_counter()


# In[149]:


type(fight)


# In[150]:


fight("kimura")


# In[151]:


fight("knee_bar")


# In[152]:


fight("keylock")


# In[153]:


from functools import partial

def multiple_attacks(attack_one, attack_two):
  """Performs two attacks"""
  
  print(f"First Attack {attack_one}")
  print(f"Second Attack {attack_two}")
  
attack_this = partial(multiple_attacks, "kimura")
type(attack_this)


# In[154]:


attack_this("knee-bar")


# In[155]:


multiple_attacks("Darce Choke", "Bicep Slicer")


# In[156]:


def lazy_return_random_attacks():
    """Yield attacks each time"""
    import random
    attacks = {"kimura": "upper_body",
           "straight_ankle_lock":"lower_body", 
           "arm_triangle":"upper_body",
            "keylock": "upper_body",
            "knee_bar": "lower_body"}
    while True:
        random_attack = random.choices(list(attacks.keys()))
        yield random_attack


# In[157]:


attack = lazy_return_random_attacks()


# In[158]:


type(attack)


# In[159]:


for _ in range(6):
    print(next(attack))


# In[160]:


def randomized_speed_attack_decorator(function):
    """Randomizes the speed of attacks"""
    
    import time
    import random
    
    def wrapper_func(*args, **kwargs):
        sleep_time = random.randint(0,3)
        print(f"Attacking after {sleep_time} seconds")
        time.sleep(sleep_time)
        return function(*args, **kwargs)
    return wrapper_func


# In[161]:


@randomized_speed_attack_decorator
def lazy_return_random_attacks():
    """Yield attacks each time"""
    import random
    attacks = {"kimura": "upper_body",
           "straight_ankle_lock":"lower_body", 
           "arm_triangle":"upper_body",
            "keylock": "upper_body",
            "knee_bar": "lower_body"}
    while True:
        random_attack = random.choices(list(attacks.keys()))
        yield random_attack


# In[162]:


for _ in range(5):
    print(next(lazy_return_random_attacks()))


# In[163]:


from functools import wraps
from time import time

def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time()
        result = f(*args, **kw)
        te = time()
        print(f"fun: {f.__name__}, args: [{args}, {kw}] took: {te-ts} sec")
        return result
    return wrap


# In[164]:


@timing
def some_attacks(foo):
  print(f"this was passed in: {foo}")
  attack = lazy_return_random_attacks()
  for _ in range(5):
    print(next(attack))
    
some_attacks("bar")


# In[165]:


class AttackFinder:
  """Finds the attack location"""
  
  
  def __init__(self, attack):
    self.attack = attack
  
  def __call__(self):
    attacks = {"kimura": "upper_body",
           "straight_ankle_lock":"lower_body", 
           "arm_triangle":"upper_body",
            "keylock": "upper_body",
            "knee_bar": "lower_body"}
    if not self.attack in attacks:
      return "unknown location"
    return attacks[self.attack]


# In[166]:


my_attack = AttackFinder("kimura")
my_attack()


# In[167]:


import pandas as pd
iris = pd.read_csv('https://raw.githubusercontent.com/mwaskom/seaborn-data/master/iris.csv')
iris.head(3)


# In[168]:


iris.shape


# In[169]:


iris['rounded_sepal_length'] = iris[['sepal_length']].apply(pd.Series.round)
iris.head()


# In[170]:


iris.shape


# In[171]:


def multiply_by_100(x):
    """Multiplies by 100"""
    
    res = x * 100
    #print(f"This was passed in {x}, and this result was generated {res}")
    return res
  
  
iris['100x_sepal_length'] = iris[['sepal_length']].apply(multiply_by_100)
iris.head()


# In[172]:


iris["new_column"] = iris[['sepal_length']]
iris.head()


# In[173]:


iris.groupby("species").max()


# In[175]:


#iris.apply(pd.Series.round, axis=1)


# In[176]:


#def sepal_category(x):
  
#  if x == 4:
#  return "big"

#iris['sepal_category'] = iris[['sepal_width']].apply(sepal_category)
#iris.head()


# In[177]:


#example of a smarter function
def smart_multiply_by_100(x):
  if x > 5:
    return 1
  return x

inputs = [1,2,6,10]
for input in inputs:
  print(smart_multiply_by_100(input))


# In[178]:


import this


# In[179]:


func = lambda x: x**2
func(4)


# In[180]:


def regular_func(x):
  return x**2

regular_func(4)


# In[181]:


def regular_func2(x):
  """This makes my variable go to the second power"""
  return x**2


# In[182]:


regular_func2(2)


# In[ ]:





# In[183]:


import random


# In[184]:


random.seed


# In[185]:


def forever():
    while True:
        yield 1


# In[186]:


res = forever()
type(res)


# In[187]:


next(res)


# In[188]:


#Finite
def not_forever():
    vars = [1,2,3,5]
    for var in vars:
        yield var
    


# In[189]:


res2 = not_forever()


# In[190]:


next(res2)


# In[191]:


get_ipython().system('ls -l')


# In[192]:


directory_listing = get_ipython().getoutput('ls -l')
type(directory_listing)


# In[194]:


dir /l


# In[195]:


directory_listing = get_ipython().getoutput('ls -l')
type(directory_listing)


# In[196]:


directory_listing.grep(".py")


# In[197]:


var = """This is a a very long string
 and it needs to be continued"""


# In[198]:


long_list = [12,3,5,
             6,7,7,8]


# In[199]:


#\


# In[200]:


f = open('workfile.txt', 'w')
f.write("foo20\n")
f.write("foo3\n")
f.write("foo4\n")
f.close()
get_ipython().system('cat workfile.txt')

#!ls -l


# In[205]:


my_writes = ["foo2\n","foo3\n","foo4\n"]
f = open('workfile2.txt', 'w')
for line in my_writes:
  f.write(line)
f.close()
get_ipython().system('cat workfile2.txt')


# In[206]:


with open("workfile.txt", "w") as workfile:
    workfile.write("bam")
get_ipython().system('cat workfile.txt')


# In[207]:


f = open("workfile.txt", "r")
out = f.readlines() #r.read() works as well
# Maybe we want to create a generator pipeline
for line in out:
  print(line)
  #  yield line
f.close()
print(out)


# In[208]:


with open("workfile.txt", "r") as workfile:
    print(workfile.readlines())
    #print(workfile.read())


# In[209]:


mydict = {"one":1, "two":2}


# In[210]:


import pickle


# In[211]:


pickle.dump(mydict, open('mydictionary.pickle', 'wb'))


# In[212]:


get_ipython().system('ls -lh mydictionary.pickle')


# In[213]:


#!cat mydictionary.pickle


# In[214]:


res = pickle.load(open('mydictionary.pickle', "rb"))


# In[215]:


print(res)


# In[216]:


import json
with open('data.json', 'w') as outfile:
    json.dump(res, outfile)


# In[217]:


get_ipython().system('cat data.json')


# In[218]:


with open('data.json', 'rb') as outfile:
    res2 = json.load(outfile)


# In[219]:


print(res2)
type(res2)


# In[220]:


import yaml


# In[221]:


with open("data.yaml", "w") as yamlfile:                                               
    yaml.safe_dump(res2, yamlfile, default_flow_style=False)


# In[222]:


get_ipython().system('cat data.yaml')


# In[223]:


with open("data.yaml", "rb") as yamlfile:                                               
    res3 = yaml.safe_load(yamlfile) 


# In[224]:


print(res3)
type(res3)


# In[225]:


new_variable = {"IP_Address": "Hostname"}
with open("hosts.yaml", "w") as yamlfile:
     yaml.safe_dump(new_variable, yamlfile, default_flow_style=False)


# In[226]:


get_ipython().system(' cat hosts.yaml')


# In[227]:


var = {"one":"two"}
pickle.dump(var, open('unreadable.pickle', 'wb'))


# In[228]:


get_ipython().system('ls -lh unreadable.pickle')


# In[229]:


with open('var.json', 'w') as outfile:
    json.dump(var, outfile)


# In[230]:


get_ipython().system('ls -lh var.json')


# In[231]:


# Make some dirs
import pathlib
import os

path = "somedir"
#Depending on the script you are writing maybe you want this False
pathlib.Path(path).mkdir(parents=True, exist_ok=True)


# In[232]:


ls -la somedir


# In[233]:


import pathlib
pathvar = pathlib.Path("newfile.txt")
pathvar2 = pathlib.Path("somedir")


# In[234]:


pathvar.exists()


# In[235]:


pathvar2.exists()


# In[236]:


pathvar.write_text("foo")


# In[237]:


cat newfile.txt


# In[238]:


from pathlib import Path
p = Path('foo.txt')
p.open('w').write('bam')

#This file is created
print("Before rename")
get_ipython().system('ls -l *.txt')

out = Path('bar.txt')
p.rename(out)

#It is renamed
print("After rename")
get_ipython().system('ls -l *.txt')
#!rm bar.txt


# In[239]:


cat bar.txt


# In[240]:


mypath = Path('bar.txt')


# In[241]:


mypath.absolute()


# In[242]:


mypath.cwd()


# In[243]:


import fnmatch

files = ["data1.csv", "script.py", "image.png", "data2.csv", "all.py"]


def csv_matches(files):
    """Return matches for csv files"""

    matches = fnmatch.filter(files, "*.csv")
    return matches


# Print matches to pattern
print(f"Found matches: {csv_matches(files)}")


# In[244]:


from shutil import make_archive
import os

username = "user1"
root_dir = "/tmp"
# archive path
path = f"{root_dir}/{username}"

# create tar and gzipped archive
make_archive(username, "gztar", root_dir)

# create zip archive
make_archive(username, "zip", root_dir)

print(os.listdir("/tmp"))


# In[245]:


import os
from os.path import join, getsize
for root, dirs, files in os.walk('somedir'):
    print(root, "consumes", end=" ")
    print(sum(getsize(join(root, name)) for name in files), end=" ")
    print("bytes in", len(files), "non-directory files")
    if 'CVS' in dirs:
        dirs.remove('CVS')  # don't visit CVS directories


# In[246]:


import os
from os.path import join, getsize
for root, dirs, files in os.walk('sample_data'):
    print(f"files I found: {files}")
    #full path
    for name in files:
        full_path = join(root, name)
        print(f"full path to files: {full_path}")


# In[247]:


import os
location = "sample_data/anscombe.json"
result = os.stat(location)


# In[248]:


result.st_gid


# In[249]:


result.st_size


# In[250]:


import subprocess
import shlex

print("Enter a list of directories to calculate storage total: \n")
user_input = "pluto mars jupyter"
sanitized_user_input = shlex.split(user_input)
print(f"raw_user_input: {user_input} |  sanitized_user_input: {sanitized_user_input}")
cmd = ["du", "-sh", "--total"]
cmd.extend(sanitized_user_input)
print(f"cmd: {cmd}")
disk_total = subprocess.run(cmd, stdout=subprocess.PIPE)
print(disk_total)


# In[251]:


import subprocess
import os

# setup
file_location = "/tmp/file.txt"
expected_uid = 501
# touch a file
proc = subprocess.Popen(["touch", file_location])

# check user permissions
stat = os.stat(file_location)


# In[252]:


import subprocess

print("Enter a path to search for directories: \n")
user_input = input()
print(f"user_input: {user_input}")
with subprocess.Popen(
    ["find", user_input, "-type", "d"], stdout=subprocess.PIPE
) as find:
    result = find.stdout.readlines()
    for line in result:
        print(f"Found Directory: {line}")


# In[253]:


get_ipython().system('touch foo.csv && touch bar.txt')
import glob
glob.glob("*.csv")


# In[254]:


path = "sample_data"
results = glob.glob(f"{path}/*.csv")

#how many many CSV did you find?
print(len(results))
results


# In[255]:


path = "sample_data"
results = glob.glob(f"{path}/*.json")

#how many many CSV did you find?
print(len(results))
results


# In[256]:


get_ipython().run_cell_magic('writefile', 'hello-click.py', "import click\n\n@click.command()\ndef hello():\n    click.echo('Hello World!')\n\nif __name__ == '__main__':\n    hello()\n")


# In[257]:


get_ipython().system('python hello-click.pyv')


# In[258]:


get_ipython().system('python hello-click.py')


# In[ ]:




